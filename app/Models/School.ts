import { DateTime } from 'luxon'
import { BaseModel, column, ManyToMany, manyToMany } from '@ioc:Adonis/Lucid/Orm'
import Account from './Account';

export default class School extends BaseModel {
  @column({ isPrimary: true })
  public id: string;

  @column()
  public teritoryCode: string;

  @column()
  public parentSchoolId?: string;

  @column()
  public name: string;
  
  @column()
  public address: string;

  @column()
  public licenseNumber?: string;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoUpdate: true })
  public updatedAt?: DateTime

  @column.dateTime()
  public deletedAt?: DateTime

  @manyToMany(() => Account, {
    pivotTable: 'school_accounts',
    pivotForeignKey: 'school_id',
    pivotRelatedForeignKey: 'account_id',
  })
  accounts: ManyToMany<typeof Account>;
}
