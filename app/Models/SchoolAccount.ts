import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class SchoolAccount extends BaseModel {
  @column({ isPrimary: true })
  public id: string;

  @column()
  public schoolId: string;

  @column()
  public roleId: string;

  @column()
  public accountId: string;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoUpdate: true })
  public updatedAt?: DateTime

  @column.dateTime()
  public deletedAt?: DateTime
}
