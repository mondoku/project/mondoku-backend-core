import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class TaskScoring extends BaseModel {
  @column({ isPrimary: true })
  public id: string;

  @column()
  public assignmentId: string;

  @column()
  public score: number;

  @column()
  public notes?: string;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoUpdate: true })
  public updatedAt?: DateTime

  @column.dateTime()
  public deletedAt?: DateTime
}
