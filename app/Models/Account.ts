import { DateTime } from 'luxon'
import { BaseModel, column, ManyToMany, manyToMany } from '@ioc:Adonis/Lucid/Orm'
import Hash from '@ioc:Adonis/Core/Hash';
import Role from './Role';

export default class Account extends BaseModel {
  @column({ isPrimary: true })
  public id: string;

  @column()
  public email: string;

  @column()
  public password: string;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoUpdate: true })
  public updatedAt?: DateTime

  @column.dateTime()
  public deletedAt?: DateTime

  @manyToMany(() => Role, {
    pivotTable: 'school_accounts',
    pivotForeignKey: 'account_id',
    pivotRelatedForeignKey: 'role_id',
  })
  roles: ManyToMany<typeof Role>;
}

export const hashPassword = (password: string) => {
  return Hash.make(password);
}

export const checkPassword = (hased: string, plain: string) => {
  return Hash.verify(hased, plain);
}
