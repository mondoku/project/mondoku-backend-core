import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class TaskAssignment extends BaseModel {
  @column({ isPrimary: true })
  public id: string;

  @column()
  public taskId: string;

  @column()
  public assigneeId: string;

  @column()
  public assigneeType: string;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoUpdate: true })
  public updatedAt?: DateTime

  @column.dateTime()
  public deletedAt?: DateTime
}
