import Role, { RoleType } from 'App/Models/Role';
import School from 'App/Models/School';

export default class SchoolsController {
  public async index(userId: string) {
    const q = School.query()
      .preload('accounts', q => q.select('id', 'email').preload('roles', q => q.select('id', 'name').first()))
      .whereHas('accounts', q => q.where('accounts.id', userId).whereHas('roles', q => q.where('name', 'admin')));

    return q;
  }

  public async store(userId: string, school: School) {
    const newSchool = await school.save();
    const admin = await Role.query().where('name', RoleType.ADMIN).first();
    await newSchool.related('accounts').attach({[userId]: { role_id: admin?.id }});
    return newSchool;
  }

  public async show(id: string) {}

  public async update() {}

  public async destroy(id: string) {}
}
