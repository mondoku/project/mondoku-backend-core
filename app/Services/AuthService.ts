import Account, { checkPassword, hashPassword } from "App/Models/Account";

export default class AuthService {

  async login(email: string, password: string): Promise<Account> {
    try {
      const account = await Account.findByOrFail('email', email);
      const passwordValid = await checkPassword(account.password, password);
      if (!passwordValid) {
        throw Error();
      }
      return account;
    } catch (error) {
      throw error;
    }
  }

  async register(email: string, password: string) {
    const account = new Account();
    account.email = email;
    account.password = await hashPassword(password);
    await account.save();
    return account;
  }

}
