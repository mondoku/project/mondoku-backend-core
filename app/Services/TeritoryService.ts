import Teritory from "App/Models/Teritory";

export default class TeritoryService {

  getAllProvinces() {
    return Teritory.query()
      .whereRaw('LENGTH(code) = 2')
      .orderBy('name');
  }

  getAllDistricts(province_code: string) {
    return Teritory.query()
      .whereRaw('LENGTH(code) = 5 AND LEFT(code, 2) = ?', [province_code])
      .orderBy('name');
  }

  getAllSubdistricts(district_code: string) {
    return Teritory.query()
      .whereRaw('LENGTH(code) = 8 AND LEFT(code, 5) = ?', [district_code])
      .orderBy('name');
  }

}
