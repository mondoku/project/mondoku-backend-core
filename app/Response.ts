import type { ResponseContract } from '@ioc:Adonis/Core/Response';


class Response {

  constructor(
    private response: ResponseContract,
    public status: number = 200,
    public message: string = 'ok',
    public data: any | null = null,
  ) {
  }

  send() {
    this.response.status(this.status);
    this.response.send({
      message: this.message,
      data: this.data,
    });
  }

}

export const ResponseOK = (
  response: ResponseContract,
  data: any | null = null,
) => {
  new Response(response, 200, 'ok', data).send();
};
