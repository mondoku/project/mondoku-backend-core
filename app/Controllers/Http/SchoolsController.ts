import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import services from '@ioc:App/Services';
import School from 'App/Models/School';
import { ResponseOK } from 'App/Response'

export default class SchoolsController {
  public async index({ response, auth }: HttpContextContract) {
    return ResponseOK(response, await services.school.index(auth.user?.id ?? ''));
  }

  public async store({ request, response, auth }: HttpContextContract) {
    const schoolData = request.all();

    const school = new School();
    school.fill(schoolData);
    return ResponseOK(response, await services.school.store(auth.user?.id ?? '', school))
  }

  public async show({}: HttpContextContract) {}

  public async update({}: HttpContextContract) {}

  public async destroy({}: HttpContextContract) {}
}
