import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import BadRequestException from 'App/Exceptions/BadRequestException';
import UnauthorizedException from 'App/Exceptions/UnauthorizedException';
import { ResponseOK } from 'App/Response';
import Services from '@ioc:App/Services';

export default class AuthController {

  async login({ request, response, auth }: HttpContextContract) {
    const { email, password } = request.all();

    try {
      const account = await Services.auth.login(email, password);
      const token = await auth.use('api').login(account);
      return ResponseOK(response, token);
    } catch (error) {
      throw new UnauthorizedException('invalid email or password');
    }
  }

  async register({ request, response }: HttpContextContract) {
    const { email, password } = request.all();

    try {
      const account = await Services.auth.register(email, password);
      return ResponseOK(response, account.email);
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async info({ response, auth }: HttpContextContract) {
    return ResponseOK(response, {
      id: auth.user?.id,
      email: auth.user?.email,
      createdAt: auth.user?.createdAt,
    });
  }

}
