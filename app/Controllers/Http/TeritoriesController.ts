import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import { ResponseOK } from 'App/Response';
import Service from '@ioc:App/Services';
import BadRequestException from 'App/Exceptions/BadRequestException';

export default class TeritoriesController {
  public async getAllProvinces({ response }: HttpContextContract) {
    const provinces = await Service.teritory.getAllProvinces();

    return ResponseOK(response, provinces);
  }

  public async getAllDistricts({ request, response }: HttpContextContract) {

    const { province: province_code } = request.all();
    if (!province_code) {
      throw new BadRequestException('province can not be empty');
    }
  
    const districts = await Service.teritory.getAllDistricts(province_code);
    return ResponseOK(response, districts);

  }

  public async getAllSubdistricts({ request, response }: HttpContextContract) {

    const { district: district_code } = request.all();
    if (!district_code) {
      throw new BadRequestException('district can not be empty');
    }

    const subdistricts = await Service.teritory.getAllSubdistricts(district_code);
    return ResponseOK(response, subdistricts);

  }
}
