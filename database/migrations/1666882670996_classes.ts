import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'classes'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.raw('uuid_generate_v4()'));
      table.uuid('class_group_id');

      table.text('name').notNullable();

      table.timestamp('created_at', { useTz: false }).defaultTo(this.now());
      table.timestamp('updated_at', { useTz: false });
      table.timestamp('deleted_at', { useTz: false });

      table.foreign('class_group_id').references('class_groups.id');
    });
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
