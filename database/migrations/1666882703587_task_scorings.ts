import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'task_scorings'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.raw('uuid_generate_v4()'));
      table.uuid('assignment_id').notNullable();

      table.integer('score').checkPositive().notNullable();
      table.text('notes');

      table.timestamp('created_at', { useTz: false }).defaultTo(this.now());
      table.timestamp('updated_at', { useTz: false });
      table.timestamp('deleted_at', { useTz: false });

      table.foreign('assignment_id').references('task_assignments.id');
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
