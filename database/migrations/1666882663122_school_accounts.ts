import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'school_accounts'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.raw('uuid_generate_v4()'));
      table.uuid('school_id').notNullable();
      table.uuid('account_id').notNullable();
      table.uuid('role_id').notNullable();

      table.foreign('school_id').references('schools.id');
      table.foreign('account_id').references('accounts.id');
      table.foreign('role_id').references('roles.id');
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
