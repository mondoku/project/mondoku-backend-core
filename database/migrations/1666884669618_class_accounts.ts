import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'class_accounts'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.raw('uuid_generate_v4()'));
      table.uuid('class_id').notNullable();
      table.uuid('account_id').notNullable();

      table.foreign('class_id').references('classes.id');
      table.foreign('account_id').references('accounts.id');
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
