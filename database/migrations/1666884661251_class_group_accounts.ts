import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'class_group_accounts'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.raw('uuid_generate_v4()'));
      table.uuid('class_group_id').notNullable();
      table.uuid('account_id').notNullable();

      table.foreign('class_group_id').references('class_groups.id');
      table.foreign('account_id').references('accounts.id');
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
