import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'task_assignments'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.raw('uuid_generate_v4()'));
      table.uuid('task_id').notNullable();
      table.uuid('assignee_id').notNullable();
      table.uuid('assignee_type').notNullable();

      table.timestamp('created_at', { useTz: false }).defaultTo(this.now());
      table.timestamp('updated_at', { useTz: false });
      table.timestamp('deleted_at', { useTz: false });

      table.foreign('task_id').references('tasks.id');
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
