import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'permissions'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.raw('uuid_generate_v4()'));

      table.text('entity').notNullable();
      table.text('action').notNullable();
    });
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
