import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class extends BaseSchema {
  protected tableName = 'schools'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.raw('uuid_generate_v4()'));
      table.string('teritory_code').notNullable();
      table.uuid('parent_school_id');

      table.text('name').notNullable();
      table.text('address').notNullable();
      table.text('license_number');

      table.timestamp('created_at', { useTz: false }).defaultTo(this.now());
      table.timestamp('updated_at', { useTz: false });
      table.timestamp('deleted_at', { useTz: false });

      table.foreign('teritory_code').references('teritories.code');
      table.foreign('parent_school_id').references('schools.id');
    });
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
