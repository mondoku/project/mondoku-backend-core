import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'teritories'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.text('code').unique().primary();
      table.text('name').notNullable();
    });
  }

  public async down () {
    this.schema.dropTable(this.tableName);
  }
}
