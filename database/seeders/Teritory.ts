import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import fs from 'fs';
import path from 'path';

export default class extends BaseSeeder {
  public async run () {

    const sqlString = fs.readFileSync(path.join(__dirname, './teritory.sql'));
    await this.client.rawQuery(sqlString.toString()).exec();

  }
}
