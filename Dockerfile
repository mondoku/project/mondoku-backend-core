FROM node:lts-alpine AS builder

WORKDIR /app

COPY package.json .
COPY pnpm-lock.yaml .

RUN npm i -g pnpm && \
    pnpm i && \
    node ace build --production

FROM node:lts-alpine

WORKDIR /app

COPY package.json .
COPY pnpm-lock.yaml .
COPY --from=builder /app/build ./

RUN npm i -g pnpm && \
    pnpm i --production

CMD ['node', 'server.js']
