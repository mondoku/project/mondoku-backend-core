declare module '@ioc:App/Services' {
  import { Services } from "Providers/ServiceProvider";

  const services: Services;
  export default services;

}
