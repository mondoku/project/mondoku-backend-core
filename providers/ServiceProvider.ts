import type { ApplicationContract } from '@ioc:Adonis/Core/Application'

/*
|--------------------------------------------------------------------------
| Provider
|--------------------------------------------------------------------------
|
| Your application is not ready when this file is loaded by the framework.
| Hence, the top level imports relying on the IoC container will not work.
| You must import them inside the life-cycle methods defined inside
| the provider class.
|
| @example:
|
| public async ready () {
|   const Database = this.app.container.resolveBinding('Adonis/Lucid/Database')
|   const Event = this.app.container.resolveBinding('Adonis/Core/Event')
|   Event.on('db:query', Database.prettyPrint)
| }
|
*/

import TeritoryService from 'App/Services/TeritoryService';
import AuthService from 'App/Services/AuthService';
import SchoolService from 'App/Services/SchoolService';
export class Services {

  constructor(
    public teritory: TeritoryService,
    public auth: AuthService,
    public school: SchoolService,
  ) {
  }

}

export default class ServiceProvider {
  constructor(protected app: ApplicationContract) {}

  public register() {
    this.app.container.singleton('App/Services', () => {
      return new Services(
        new (require('../app/Services/TeritoryService').default)(),
        new (require('../app/Services/AuthService').default)(),
        new (require('../app/Services/SchoolService').default)(),
      );
    });
  }

  public async boot() {
    // All bindings are ready, feel free to use them
  }

  public async ready() {
    // App is ready
  }

  public async shutdown() {
    // Cleanup, since app is going down
  }
}
