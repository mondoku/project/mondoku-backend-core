import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})

Route.group(() => {
  Route.get('provinces', 'TeritoriesController.getAllProvinces').as('province.index');
  Route.get('districts', 'TeritoriesController.getAllDistricts').as('district.index');
  Route.get('subdistricts', 'TeritoriesController.getAllSubdistricts').as('subdistrict.index');
  // Route.get('subdistricts/:district_code', 'TeritoriesController.getSubdistrictByID').as('subdistrict.show');
});

Route.group(() => {
  Route.post('/login', 'AuthController.login').as('auth.login');
  Route.post('/register', 'AuthController.register').as('auth.register');
  Route.get('/info', 'AuthController.info').middleware('auth').as('auth.info');
}).prefix('/auth');

Route.group(() => {
  Route.resource('schools', 'SchoolsController')
    .apiOnly();
})
  .middleware('auth');

